package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"
)

type problem struct {
	q string
	a string
}

func main() {
	csvFilename, timeLimit := parseArguments()
	problems := getProblemsFromFile(csvFilename)
	runQuiz(problems, timeLimit)
	fmt.Println("press Enter to exit quiz")
	fmt.Scanln() // wait for hitting the enter key to end the program
}

func parseArguments() (*string, *int) {
	csvFilename := flag.String("csv", "problems.csv", "a csv file in the format of 'question,answer'")
	timeLimit := flag.Int("limit", 30, "the time limit for the quiz in seconds")
	flag.Parse()
	return csvFilename, timeLimit
}

func getProblemsFromFile(csvFilename *string) (res []problem) {
	file, err := os.Open(*csvFilename)
	if err != nil {
		exit(fmt.Sprintf("Failed to open CSV file: %s", *csvFilename))
		return
	}
	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		exit(fmt.Sprintf("Failed to parse the provided CSV file."))
		return
	}
	res = parseLines(lines)
	return
}

func runQuiz(problems []problem, timeLimit *int) {
	tl := time.Duration(*timeLimit)
	timer := time.NewTimer(tl * time.Second)
	_ = timer
	correct := 0

	for i, p := range problems {
		fmt.Printf("Problem #%d: %s = ", i+1, p.q)
		answerCh := make(chan string)
		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerCh <- answer
		}()

		select {
		case <-timer.C:
			fmt.Printf("\nTime's up :(\n")
			goto End
		case answer := <-answerCh:
			correct = checkAnswer(answer, p, correct)
		}

	}

End:
	fmt.Printf("You scored %d out of %d.\n", correct, len(problems))
}

func checkAnswer(answer string, p problem, correct int) int {
	if answer == p.a {
		correct++
		fmt.Println("Correct!")
	}
	return correct
}

func parseLines(lines [][]string) []problem {
	ret := make([]problem, len(lines))
	for i, line := range lines {
		ret[i] = problem{
			q: line[0],
			a: strings.TrimSpace(line[1]),
		}
	}
	return ret
}

var osExit = os.Exit

func exit(msg string) {
	fmt.Println(msg)
	osExit(1)
}
